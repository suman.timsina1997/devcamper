const nodemailer = require("nodemailer");

const sendEmail = async (options) => {
  //create reuseable transport object
  let transporter = nodemailer.createTransport({
    host: "smtp-relay.sendinblue.com", //process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    auth: {
      user: "suman.timsina1997@gmail.com", //process.env.SMTP_EMAIL,
      pass: "x8mL1vAZ0wyXG2cO", //process.env.SMTP_PASSWORD,
    },
  });

  // send mail with defined transport object
  const message = {
    from: `${process.env.FROM_NAME}<${process.env.FROM_EMAIL}>`,
    to: options.email,
    subject: options.subject,
    text: options.message,
  };
  const info = await transporter.sendMail(message);
  console.log("Message sent: %s", info.messageId);
};

module.exports = sendEmail;
